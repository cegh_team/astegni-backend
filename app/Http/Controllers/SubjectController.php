<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Resources\SubjectCollection;
use App\Http\Resources\Subject as SubjectResource;

class SubjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new SubjectCollection(Subject::all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'=>'required',
            'type'=>'required',
        ]);
        
        $subject = Subject::create($data);

        return response()->json([
            'subject'=> $subject,
            'message'=> 'subject added successfully!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = Subject::find($id);

        if (is_null($subject)) {
            return response()->json(['message'=> 'subject not found.']);
        }

        return new SubjectResource($subject);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject = Subject::find($id);

        $subjectData = $request->validate([
            'name'=>'required',
            'type'=>'required',
            'woreda'=>'required',
            'house_number'=>'required',
        ]);

        $subject->name = $subjectData['name'];
        $subject->type = $subjectData['type'];
        $subject->woreda = $subjectData['woreda'];
        $subject->house_number = $subjectData['house_number'];
        $subject->save();

        return response()->json(['Subject'=> $subject]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subject::find($id)->delete();

        return response()->json(['message'=> 'Subject deleted successfully.']);
    }
}





<?php

namespace App\Http\Controllers;

use App\Events\RequestSent;
use App\Http\Resources\Request as ResourcesRequest;
use App\Http\Resources\RequestCollection;
use App\Models\ARequest;
use Illuminate\Http\Request;

class ARequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = ARequest::all();

        return new RequestCollection($requests);
    }


    public function recievedRequests($id)
    {
        //handle recieved requests for tutor here
        $requests = ARequest::where('tutor_id', $id)->where('status', null)->get();
        
        return new RequestCollection($requests);
    }

    public function sentRequests($id)
    {
        //handle sent requests for student here
        $requests = ARequest::where('student_id', $id)->get();

        return new RequestCollection($requests);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = new ARequest();
        $req->student_id = $request->student_id;
        $req->tutor_id = $request->tutor_id;
        $req->content = $request->content;
        $req->save();
        
        // $request = ARequest::create($data);

        //dispatch request sent event
        event(new RequestSent($req));
        
        return new ResourcesRequest($req);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ARequest  $aRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aRequest = ARequest::find($id);

        if (is_null($aRequest)) {
            return response()->json(['message'=> 'Request not found.']);
        }

        return new ResourcesRequest($aRequest);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ARequest  $aRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aRequest = ARequest::find($id);

        $data = [
            'content' => $request->content == null ? $aRequest->content : $request->content,
            'status' => $request->status == null ? $aRequest->status : $request->status
        ];

        $aRequest->update($data);

        return new ResourcesRequest($aRequest);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ARequest  $aRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(ARequest $req)
    {
        // print("wow");

        $req->delete();

        return response(['message'=> 'Request deleted successfully.']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Http\Resources\Message as ResourcesMessage;
use App\Http\Resources\MessageCollection;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $messages = Message::all();

      return new MessageCollection($messages);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function messages($id)
    {
      $messages = Message::where('sender_id', $id)->orWhere('reciever_id', $id)->orderBy('created_at', 'DESC')->limit(10)->get();

      return new MessageCollection($messages);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function chats($auth_id, $user_id)
    {
    //   $messages = Message::where(function($query) use ($request) {
    //     $query->where('from_user', Auth::user()->id)->where('to_user', $request->user_id);
    // })->orWhere(function ($query) use ($request) {
    //     $query->where('from_user', $request->user_id)->where('to_user', Auth::user()->id);
    // })->orderBy('created_at', 'ASC')->limit(10)->get();

      $messages = Message::where('sender_id', $auth_id)->where('reciever_id', $user_id)->get();

      return new MessageCollection($messages);
    }

    /**
 * Persist message to database
 *
 * @param  Request $request
 * @return Response
 */
public function sendMessage(Request $request, $id)
{

  $message = new Message();
  $message->sender_id = $id;
  $message->reciever_id = $request->reciever_id;
  $message->message = $request->message;
  $message->save();

  $user = User::find($message->reciever_id);

  //message sent event will broadcast over pusher
  // broadcast(new MessageSent($user, $message))->toOthers();

  return ['message' => $message];
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $message = Message::find($id);

      $data = [
          'message' => $request->message == null ? $message->message : $request->message,
          'status' => $request->status == null ? $message->status : $request->status,
      ];

      $message->update($data);

      return new ResourcesMessage($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
      $message->delete();

      return response(['message'=> 'Message deleted successfully.']);
    }
}

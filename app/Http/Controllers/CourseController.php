<?php

namespace App\Http\Controllers;

use App\Events\RequestAccepted;
use App\Http\Resources\Course as ResourcesCourse;
use App\Http\Resources\CourseCollection;
use App\Models\ARequest;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Course::all();
    }

    public function studentCourses($id)
    {
        $courses = Course::where('student_id', $id)->get();

        return new CourseCollection($courses);
    }

    public function tutorCourses($id)
    {
        $courses = Course::where('tutor_id', $id)->get();

        return new CourseCollection($courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course = new Course();
        $course->student_id = $request->student_id;
        $course->tutor_id = $request->tutor_id;
        $course->course_name = $request->course_name;
        $course->progress = $request->progress;
        $course->schedule = $request->schedule;
        $course->save();

        $req = ARequest::find($request->request_id);
        $req->status = "recieved";
        $req->save();

        //dispatch request sent event
        event(new RequestAccepted($req));

        return $course;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $courseId)
    {
        $course = Course::find($courseId);
        
        $data = [
            'course_name' => $request->course_name == null ? $course->course_name : $request->course_name,
            'progress' => $request->progress == null ? $course->progress : $request->progress,
            'schedule' => $request->schedule == null ? $course->schedule : $request->schedule
        ];

        $course->update($data);

        return new ResourcesCourse($course);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();

        return response(['message'=> 'Course deleted successfully.']);
    }
}

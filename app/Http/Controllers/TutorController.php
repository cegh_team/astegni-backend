<?php

namespace App\Http\Controllers;

use Amranidev\Laracombee\Laracombee;
use App\Models\Tutor;
use Illuminate\Http\Request;
use App\Http\Resources\User as ResourcesUser;
use App\Http\Resources\UserCollection;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class   TutorController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tutors = User::whereHas('role', function($query){
            return $query->where('role', 'tutor');
        })->get();

        return new UserCollection($tutors);

        // return $tutors;
    }

    public function activeTutors($id){

        // $user = User::findOrFail($id);

        // $client = new Laracombee();

        // Prepare the request for recombee server, we need 10 recommended items for a given user

        // $recommendations = $client->recommendTo($user, 20)->wait();

        // $itemsIds = $recommendations['recomms']; // [2, 5, 55, 24, 32, 91]

        // $items = User::find($itemsIds);

        // $itms = new UserCollection($items);

        // $tutors = [];

        // foreach ($itms as $item) {
        //     error_log($item->role->role);

        //    if( $item->role->role == 'tutor' and $item->status == 'true'){
        //        array_push($tutors, $item);
        //    }
        // }

        // return ['response' =>$items]; 

        $tutors = User::where("status", "true")->whereHas('role', function($query){
            return $query->where('role', 'tutor');
        })->get();

        return new UserCollection($tutors);
    }

    public function search(Request $request,$filter,$term){

         // $tutors = User::where('first_name','like',"%{$term}%")
          //->orWhere('last_name', 'like', "%{$term}%")->get();
        if($filter=='name'){

              $tutors = User::where('first_name','like',"%{$term}%")
          ->orWhere('last_name', 'like', "%{$term}%")->get();
        }
        else{
            $tutors = User::where("{$filter}",'like',"%{$term}%")->get();
        }

          return new UserCollection($tutors);



    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'bio'=>'required',
            'teaching_level'=>'required',
            'pricing'=>'required',
            'availability'=>'required',
            'occupation' => 'required',
        ]);

        $tutor = Tutor::create($data);

        return response()->json([
            'tutor'=> $tutor,
            'message'=> 'tutor added successfully!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tutor  $tutor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tutor = User::find($id);

        if (is_null($tutor)) {
            return response()->json(['message'=> 'tutor not found.']);
        }

        return new ResourcesUser($tutor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tutor  $tutor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tutor = DB::table('tutors')->where('user_id', $id)->first();
        $user = User::find($id);
        if(is_null($tutor)){

            $tut = new Tutor;
            $tut->bio = $request->bio;
            $tut->teaching_level = $request->teaching_level;
            $tut->pricing = $request->pricing;
            $tut->availability = $request->availability;
            $tut->occupation = $request->occupation;
            $tut->gender = $request->gender;
            $user->tutor()->save($tut);
        }else{
            $data = [
                'bio' => $request->bio == null ? $tutor->bio : $request->bio,
                'teaching_level' => $request->teaching_level == null ? $tutor->teaching_level : $request->teaching_level,
                'pricing' => $request->pricing == null ? $tutor->pricing : $request->pricing,
                'availability' => $request->availability == null ? $tutor->availability : $request->availability,
                'occupation' => $request->occupation == null ? $tutor->occupation : $request->occupation,
                'gender' => $request->gender == null ? $tutor->gender : $request->gender
            ];

            DB::table('tutors')->where('user_id', $id)->update($data);
        }

        return response()->json(['user'=> new ResourcesUser($user)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tutor  $tutor
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        $user->delete();
        
        return response()->json(['message'=> 'Tutor deleted successfully.']);
    }
}





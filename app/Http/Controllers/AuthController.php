<?php

namespace App\Http\Controllers;

use Amranidev\Laracombee\Laracombee;
use App\Http\Controllers\Controller;
use App\Http\Resources\User as ResourcesUser;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Validator;
use Carbon\Carbon;
use Dotenv\Validator as DotenvValidator;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function user(Request $request)
    {
        return new ResourcesUser($request->user());
        // return $request->user();
    }

    public function register(Request $request)
    {
        //Validate all requested data

        $validator = Validator::make($request->all(), [
            'first_name'=>'required|max:55',
            'last_name'=>'required|max:55',
            'email'=>'email|required|unique:users',
            'password'=>'required|confirmed|min:6',
        ]);
         //'password'=>'required|confirmed|min:8',

        if($validator->fails()){
            return response(['message' => $validator->errors(),422]);
        }

        $data = [
            'first_name'=> $request->first_name,
            'last_name'=> $request->last_name,
            'email'=> $request->email,
            'status'=> 'false',
            'rating'=>'0',
            'password'=> $request->password,
            'password_confirmation'=> $request->password_confirmation,
        ];

        $data['password'] = bcrypt($request->password);

        $user = User::create($data);

        ///////add user as User if role is student and Item if tutor for recommedation
        $client = new Laracombee();
        if ($request->role == 'student') {
            $client->send($client->addUser($user));
            error_log($request->role);
        }else if($request->role == 'tutor'){
            $client->send($client->addItem($user));
        }

        $role = new Role();
        $role->role = $request->role;

        $user->role()->save($role);


        $accessToken = $user->createToken('authToken', [$request->role])->accessToken;

        // $accessToken->expires_at = Carbon::now()->addWeeks(1);
        // $accessToken->save();

        return response(
            [
                'user'=>new ResourcesUser($user),
                'token'=>$accessToken,
                // 'expiresAt'=>Carbon::parse($accessToken->expires_at)->toDateTimeString()
            ], 200);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=>'email|required',
            'password'=>'required',
        ]);

        //check if validation passes
        if($validator->fails()){
            return response(['message' => $validator->errors()], 422);
        }

        //check if database is connected
        // if (!DB::connection('mysql')->getDatabaseName())
        // {
        //     return response(['message'=>'DB server is down!']);
        // }

        //attempt login with validated data
        if(!auth()->attempt($request->all())){
            return response(['message'=>'Invalid credentials'], 401);
        }

        $user = auth()->user();

        //when ever you want to assign role
        $userRole = $user->role()->first();

        if ($userRole) {
            $this->scope = $userRole->role;
        }

        //create access token
        $accessToken = $user->createToken('authToken', [$this->scope])->accessToken;
        // $accessToken = $user->createToken('authToken')->accessToken;

        //return json response
        return response(
            [
                'user'=>new ResourcesUser($user),
                'token'=>$accessToken,
            ], 200);
    }

    public function logout(Request $request){
        $request->user()->token()->revoke();

        return response('Logged out!', 200);
    }
    public function allusers(Request $request){
        $users = User::all();
       /* return response()->json([
            "success" => true,
            "message" => "Student List",
            "data" => $users
            ]);*/

            return response(
                [
                    //'users'=>new ResourcesUser($users),
                    'users'=>$users,

                ], 200);
        //
    }
}

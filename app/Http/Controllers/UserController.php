<?php

namespace App\Http\Controllers;

use Amranidev\Laracombee\Laracombee;
use App\Models\User;
use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Resources\UserCollection;
use App\Http\Resources\User as UserResource;
use App\Models\Role;
use Carbon\Carbon;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new UserCollection(User::all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'email|required|unique:users',
            'password'=>'required'
        ]);

        $data['password'] = bcrypt($data['password']);

        error_log($data['first_name']);
        
        $user = User::create($data);

        $role = new Role();
        $role->role = $request->role;

        $user->role()->save($role);

        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json(['message'=> 'User not found.']);
        }

        return new UserResource($user);
    }

    public function detailsView(Request $request){

        $client = new Laracombee();

        $resp = $client->addDetailView($request->user_id, $request->item_id,
        [
            'timestamp' => Carbon::now(),
            'cascadeCreate' => true,
          ]
        );

        $client->send($resp);

        return ['message' =>"Success!"];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        
        $data = [
            'bio' => $request->bio == null ? $user->bio : $request->bio,
            'pricing' => $request->pricing == null ? $user->pricing : $request->pricing,
            'availability' => $request->availability == null ? $user->availability : $request->availability,
            'address' => $request->address == null ? $user->address : $request->address,
            'phone' => $request->phone == null ? $user->phone : $request->phone,
            'gender' => $request->gender == null ? $user->gender : $request->gender,
            'device_token' => $request->device_token == null ? $user->device_token : $request->device_token
        ];

        $user->update($data);
        
        $subs = [];
        
        $subjects = json_decode($request->subjects);
        
        if(!($subjects==[])){

            foreach ($subjects as $sub) {
                array_push($subs, new Subject(["name" => $sub]));
            }
            
            //remove existing subjects before saving the new selection
            $user->hasMany(Subject::class)->delete();

            $user->subjects()->saveMany($subs);
        }

        ///////update user as User if role is student and Item if tutor for recommedation
        $client = new Laracombee();
        if ($user->role->role == 'student') {
            $client->updateUser($user);
        }else if($user->role->role == 'tutor'){
            $client->updateItem($user);
        }

        return new UserResource($user);
    }

    public function updateStatus(Request $request, $id){
        $user = User::find($id);

        $user->update(['status'=>$request->status]);

        return new UserResource($user);
    }

    public function updateProfile(Request $request, $id)
    {
        $imageName = time().'.'.$request->image->extension();  

        $result = $request->file('image')->storeAs('public/images/profiles', $imageName);

        $user = User::find($id);

        $user->profile_picture = $imageName;
        $user->save();

        $path = str_replace('public', 'storage', $result);
        $path = str_replace('\/', '/', $path);
        
        return response($path);
    }

    public function updateDeviceKey(Request $request, $id)
    {
        $user = User::find($id);

        $user->device_key = $request->device_key;
        $user->save();

        return response($user);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response(['message'=> 'User deleted successfully.']);
    }
}





<?php

namespace App\Http\Controllers;

use Amranidev\Laracombee\Laracombee;
use App\Http\Resources\Comment as ResourcesComment;
use App\Http\Resources\CommentCollection;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::all();

      return new CommentCollection($comments);
    }

    public function comments($id)
    {

        $comments = Comment::where('user_id', $id)->get();
        
        return new CommentCollection($comments);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comment = new Comment();
        $comment->reviewer_id = $request->reviewer_id;
        $comment->user_id = $request->user_id;
        $comment->comment = $request->comment;
        $comment->rating = $request->rating;
        $comment->save();

        $user = User::find($request->user_id);
        $rating = $this->computeRating($user->comments);
        // error_log($rating);

        $user->update(['rating'=>round($rating, 1)]);

        //addRating activity to recombee
        // $client = new Laracombee();

        // $resp = $client->addRating($request->reviewer_id, $request->user_id, $request->rating,
        // [
        //     'timestamp' => $comment->created_at,
        //     'cascadeCreate' => true,
        //   ]
        // );

        // $client->send($resp);

        return new ResourcesComment($comment);
    }

    public function computeRating($comments){
        $sum = 0;

        foreach ($comments as $comment) {
            $sum = $sum + $comment->rating;
        }

        return $sum / (float)count($comments);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);

      $data = [
          'comment' => $request->comment == null ? $comment->comment : $request->comment,
          'rating' => $request->rating == null ? $comment->rating : $request->rating,
      ];

      $comment->update($data);

      return new ResourcesComment($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

      return response(['message'=> 'Comment deleted successfully.']);
    }
}

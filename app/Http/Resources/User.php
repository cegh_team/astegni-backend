<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id' => strval($this->id),
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'profile_picture' => $this->profile_picture == null ? 'profile.jpeg' : $this->profile_picture,
            'bio' => $this->bio,
            'address' => $this->address,
            'address' => $this->address,
            'phone' => $this->phone,
            'pricing' => $this->pricing,
            'availability' => $this->availability,
            'gender' => $this->gender,
            'status' => $this->status,
            'contract_number' => $this->contract_number,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'role' => new Role($this->role),
            'subjects' => new SubjectCollection($this->subjects),
            'rating' => $this->rating==null ? "0": $this->rating,
            'device_token' => $this->device_token
        ];
    }

    
}

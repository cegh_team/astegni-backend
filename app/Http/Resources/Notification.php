<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Notification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'reciever_id'=> strval($this->reciever_id),
            'sender_id'=> strval($this->sender_id),
            'title'=> $this->title,
            'body'=> $this->body,
            'image'=> $this->image,
            'created_at'=>$this->created_at->diffForHumans()
        ];
    }
}

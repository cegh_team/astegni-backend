<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'comment'=> $this->comment,
            'rating' => number_format((double)$this->rating),
            'tutor'=> User::find($this->user_id),
            'reviewer'=> User::find($this->reviewer_id),
            'created_at' => $this->created_at->diffForHumans()
        ];
    }
}

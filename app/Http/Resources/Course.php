<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class Course extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'course_id'=> strval($this->id),
            'course_name'=> $this->course_name,
            'progress' => $this->progress,
            'schedule' => $this->schedule,
            'tutor'=> User::find($this->tutor_id),
            'student'=> User::find($this->student_id),
            'created_at' => $this->created_at->diffForHumans()
        ];
    }
}

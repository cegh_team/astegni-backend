<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class Message extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'message_id'=> strval($this->id),
            'sender_id'=> $this->sender_id,
            'reciever_id'=> $this->reciever_id,
            'message'=> $this->message,
            'sender' => User::find($this->sender_id),
            'reciever' => User::find($this->reciever_id),
            'status'=> $this->status,
            'created_at' => $this->created_at->diffForHumans()
        ];
    }
}

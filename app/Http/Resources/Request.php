<?php

namespace App\Http\Resources;

use App\Http\Resources\User as ResourcesUser;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class Request extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'request_id'=> strval($this->id),
            'content'=> $this->content,
            'tutor_id'=> $this->tutor_id,
            'status'=> $this->status,
            'student'=> User::find($this->student_id),
            'created_at' => $this->created_at->diffForHumans()
        ];
    }
}

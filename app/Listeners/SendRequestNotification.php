<?php

namespace App\Listeners;

use App\Events\RequestSent;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendRequestNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RequestSent  $event
     * @return void
     */
    public function handle(RequestSent $event)
    {
        //send notification using fcm, we get request from event here

        $request = $event->request;

        $reciever = User::find($request->tutor_id);
        $sender = User::find($request->student_id);

        $device_token =$reciever->device_token;
        // error_log($device_token);

        // $device_token = "fbYUV5LjQ9m0e2vW7eSkiZ:APA91bE9Lift8H8_PRcnzj6ylPpgZxhaJSzxX2KLiObrA8Dmcf5wG2J1GNGHDwEc4whaISBeROVFg2DpRfl0OZJuVa-cfhUpBHCpXVs069xX7pDtTC3Ex30SbPey2-1FYcRvlecWw8S8";

        $notification = new Notification();
        $notification->sender_id = $sender->id;
        $notification->reciever_id = $reciever->id;
        $notification->title = "Recieved request from ".$sender->first_name;
        $notification->body = substr($request->content, 0, 50);
        $notification->image = "no image";
        $notification->save();

        $url = 'https://fcm.googleapis.com/fcm/send';
        $serverKey = "AAAAbDuf6fg:APA91bHDgQ39K-Ki50WlYUoD3gumo3hkU1h7MiqWyXlnZVHJhCeDFhbaqEtA_E6dOvAPHM41arh1u85PvSX2SZ86kgQdKmoFx17YNy1MDjPASbmB9p7HqJ3PLJ7TahTUOOnY_HhmWCYW";
  
        $data = [
            "to" => $device_token,
            "notification" => [
                "title" => $notification->title,
                "body" => $notification->body,  
            ]
        ];
        $encodedData = json_encode($data);
    
        $headers = [
            'Authorization:key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);       
    }
}

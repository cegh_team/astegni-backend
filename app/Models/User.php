<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    public static $laracombee = ['address' => 'string', 'gender' => 'string', 'pricing' => 'string'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'bio',
        'address',
        'phone',
        'pricing',
        'availability',
        'gender',
        'role',
        'device_token',
        'rating',
        'status'
    ];

    //this are added to enable pgsql(on heroku) migration by disabling Laravel ORM auto increment
    protected $primaryKey = 'id';

    // public $incrementing = false;
    // protected $appends = ['role'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() {
        return $this->hasOne(Role::class);
    }

    public function Subjects() {
        return $this->hasMany(Subject::class);
    }

    public function notifications() {
        return $this->hasMany(Notification::class);
    }

    public function messages() {
        return $this->hasMany(Message::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function requests() {
        return $this->hasMany(ARequest::class);
    }
   
}

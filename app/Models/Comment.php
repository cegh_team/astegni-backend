<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'reviewer_id',
        'user_id',
        'comment',
        'rating'
    ];

    protected $primaryKey = 'id';
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}

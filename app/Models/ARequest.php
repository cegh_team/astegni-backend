<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ARequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'tutor_id',
        'content',
        'status'
    ];

    protected $primaryKey = 'id';

    public function user(){
        return $this->belongsTo(User::class);
    }
}

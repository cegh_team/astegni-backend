<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\User::create(['first_name' => 'chala', 'last_name' => 'megersa', 'email' => 'chala@gmail.com', 'password' => bcrypt('Pass12')]);
        $role = new \App\Models\Role();
        $role->role = 'admin';

        $user->role()->save($role);
    }
}

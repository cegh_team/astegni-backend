<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TutorController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\ARequestController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\CourseController;

Route::prefix('auth')->group(function(){
    // api/auth/register
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:api');
    Route::get('user', [AuthController::class, 'user'])->middleware('auth:api');
    Route::get('allusers', [AuthController::class, 'allusers']);

});

Route::middleware(['auth:api', 'cors'])->group(function() {
    Route::resource('subjects', SubjectController::class);
    Route::get('tutors', [TutorController::class, 'index']);
    Route::get('search/{filter}/{term}', [TutorController::class, 'search']);
    Route::resource('requests', ARequestController::class);
    Route::post('user/messages/{user}', [MessageController::class, 'sendMessage']);
    Route::get('user/messages/{senderId}', [MessageController::class, 'messages']);
    Route::get('messages/{senderId}/{recieverId}', [MessageController::class, 'chats']);
    Route::resource('messages', MessageController::class);
    Route::resource('courses', CourseController::class);
    Route::get('notifications/{user}', [NotificationController::class, 'notifications']);
    Route::resource('notifications', NotificationController::class); 
    Route::resource('tutor/comments', CommentController::class); 
    Route::get('tutors/{tutor}/comments', [CommentController::class, 'comments']); 
    Route::post('profile/{user}', [UserController::class, 'updateProfile']);
    Route::put('profile/status/{user}', [UserController::class, 'updateStatus']);
});

//attach the role middleware to the authenticated routes
Route::middleware(['auth:api', 'role', 'cors'])->group(function() {
    Route::resource('students', StudentController::class)->middleware(['scope:student']);
    Route::resource('tutors', TutorController::class)->middleware(['scope:tutor', 'scope:admin'])->except(['index', 'show']);
    Route::get('recommended/tutors/{studentId}', [TutorController::class, 'activeTutors']);
    Route::post('tutors/details', [UserController::class, 'detailsView']);
    Route::resource('users', UserController::class);//->middleware(['scope:admin']);
    Route::get('requests/{student}/sent', [ARequestController::class, 'sentRequests']);//->middleware(['scope:student']);
    Route::get('requests/{tutor}/recieved', [ARequestController::class, 'recievedRequests']);//->middleware(['scope:tutor']);
    Route::get('students/{student}/courses', [CourseController::class, 'studentCourses']);//->middleware(['scope:student']);
    Route::get('tutors/{tutor}/courses', [CourseController::class, 'tutorCourses']);//->middleware(['scope:tutor']);
});
